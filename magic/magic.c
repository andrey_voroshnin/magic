/*I don't have any includes here because all libraries are included in other files*/
/*that program depends on.*/
/* First commit */
#include "magic.h"

int main() {

	/*string to hold initial input*/
	char* temp_input = NULL;
	/*matrix and number of rows*/
	int* matrix = NULL;
	int N = 0;

	printf("Magic square checker\n");
	printf("Please enter line of integers where first N is size of matrix\n");
	printf("Where following integers are numbers in matrix less than N*N.\n");
	printf("End your input with EOF\n");

	/*get all the input. function implemented in magic_io*/
	temp_input = magic_input(temp_input);
	if (temp_input == NULL) {
		printf("Memory allocation failed or something went really bad.\n");		
		return 1;
	}

	/*parse string that we recieved earlier.*/
	/* I could implement all the logic in input function, but didn't want to overload functions */
	/* I'm also passing address of N because function returns array only.
	in first version I wrote separate function to get N, but then I've got lost with pointers and free after malloc.
	also I've reduced complexity by reducing 1 function with while loop
	parse_matrix implemented in magic_io */
	/* function returns are quite self explanatory */
	matrix = parse_matrix(temp_input, &N);
	if (matrix == NULL) {
		printf("Memory allocation failed or something went really bad.\n");	
		free(temp_input);
		return 1;
	}
	else if (matrix[0] == -1) {
		printf("Please enter positive integers only.\n");
		free(temp_input);
		free(matrix);		
		return 1;
	}
	else if (matrix[0] == -2) {
		printf("Please enter numbers not larger than %d.\n", N*N);
		free(matrix);
		free(temp_input);
		return 1;
	}
	else if (matrix[0] == -3) {
		printf("Not enough numbers entered. Please enter %d numbers for matrix.\n", N*N);
		free(matrix);
		free(temp_input);
		return 1;
	}
	else if (matrix[0] == -4) {
		printf("Too much numbers entered. Please enter less than %d numbers for matrix.\n", N*N);
		free(matrix);
		free(temp_input);
		return 1;
	}
	else if (matrix[0] == -5) {
		printf("Please enter integers greater than 0.\n");
		free(matrix);	
		free(temp_input);
		return 1;
	}
	else if (matrix[0] == -6) {
		printf("Please use matrix smaller than %d.\n",MAX_N);
		free(temp_input);
		free(matrix);
		return 1;
	}
	else if (matrix[0] == -7) {
		printf("Matrix boundaries too small. Please use matrix greater or equal to %d.\n",MIN_N);
		free(temp_input);
		free(matrix);
		return 1;
	}

	/* Check for equal numbers in given matrix */
	if (check_matrix_nums(matrix, N) == -1) {
		printf("Equal integers found. Please enter all distict integers.\n");		
		free(matrix);
		free(temp_input);
		return 1;
	}
	
	/* Check if matrix is magic quadrant
	comp_magic implemented in magic_math */
	if (comp_magic(matrix, N) == -1) {
		printf("This is not a magic square.\n");
		free(temp_input);
		free(matrix);
		return 1;
	}

	printf("You found a magic square!\nAnd it is:\n");
	print_matrix(matrix, N);
	free(matrix);
	free(temp_input);
	return 0;
}