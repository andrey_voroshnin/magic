int comp_row(const int *row, const int N);
int comp_col(const int *col, const int N);
int comp_diag1(const int *diag, const int N);
int check_matrix_nums(const int *matrix, const int N);
int comp_magic(const int* matrix, const int N);
int comp_diag2(const int *diag, const int N);