#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char* magic_input(char* temp_str);
void print_matrix(const int* matrix, const int N);
int* parse_matrix(char* temp_str,int* N);