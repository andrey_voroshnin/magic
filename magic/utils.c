#include "utils.h"

/* My function ctoi implementation because original also can return signed integers */
 int ctoi(const char chr)
{
	if (chr<'0' || chr>'9')
		return -1;
	return chr - '0';
}

/*String to integer*/
int strtoi(const char *str)
{
	int num = 0;
	int i = 0;
	while (str[i] != '\0') {
		if (ctoi(str[i]) != -1)
		{
			num = (num * 10) + ctoi(str[i]);
			i++;
		}
		else
			return -1;
	}
	return num;
}