/* STD libraries are included in magic_io.h file */
#include "magic_io.h"
#include "magic.h"
#include "utils.h"

/* get initial input string. no parsing */
char* magic_input(char* temp_str)
{
	char *ps = NULL;
	char c;
	int i = 0;
	int length = MAX_N*MAX_N*MAX_N; /*Initial possible raw input length including whitespace*/

	temp_str = (char*)malloc(length * sizeof(char));
	if (temp_str == NULL)
		return NULL;

	/*chage to EOF or '\n' depending on what you like to terminate input*/
	while ((c = getchar()) != EOF )
	{
		if (i > length) /* rellocate memory if input is very large */
		{
			length += MAX_N;
			ps = realloc(temp_str, (length) * sizeof(char));
			if (ps == NULL) {
				return NULL;
			}
			temp_str = ps;
			free(ps);
		}
		temp_str[i] = c;
		i++;
	}
	temp_str[i] = '\0';
	return temp_str;
}

/* parse string to get array of int and first N */
/* I've stayed with single array for no particular reason or maybe to reduce ammount of nested loops
also there is a lot of check and returns here because of question request to explain every error */
int* parse_matrix(char* temp_str,int* N)
{
	char *ps = NULL;
	const char token[3] = " \t\n"; /* tokens for strtok function */
	int i = 0;
	int* matrix = NULL;

	/* start strtok. I know it changes original temp_str with '/0', but from here I don't need it anymore */
	/* also I dont need this temp ps pointer... */
	ps = strtok(temp_str, token);

	while (ps != NULL)
	{
		if (i == 0) { /* initial malloc */ 
			*N=strtoi(ps); /* get integer from string. function implemented in utils */
			if (*N == -1) { /* strtoi returns -1 if string is not positive integer */
				matrix = (int*)malloc(sizeof(int)); /* this is done because function must return int* */
				matrix[0] = -1;
				return matrix;
			}
			else if (*N > MAX_N) { /* check if N is not out of boundaries */
				matrix = (int*)malloc(sizeof(int));
				matrix[0] = -6;
				return matrix;
			}
			else if (*N < MIN_N) { /* check if N is not less that 3, the minimum possible matrix */
				matrix = (int*)malloc(sizeof(int));
				matrix[0] = -7;
				return matrix;
			}

			matrix = (int*)malloc((*N)*(*N) * sizeof(int)); /* allocate exact array to hold matrix integers */
			if (matrix == NULL)
				return NULL;
			ps = strtok(NULL, token); /* advance strtok */
			i++;
			continue; /* end current while iteration */
		}
		else if (i > (*N)*(*N)) { /* check if there is not too much numbers */
			matrix[0] = -4;
			return matrix;
		}

		matrix[i-1] = strtoi(ps);
		if (matrix[i-1] == -1) { /* check if input is positive integer */
			matrix[0] = -1;
			return matrix;
		}
		else if (matrix[i-1] > (*N)*(*N)) { /* check if number is not bigger that N^2 */
			matrix[0] = -2;
			return matrix;
		}
		else if (matrix[i-1] == 0) { /* check if it is not zero */
			matrix[0] = -5;
			return matrix;
		}

		ps = strtok(NULL, token); /* advance strtok */
		i++;
	}
	if (i < (*N)*(*N)) /* check if not enough numbers entered for matrix */
		matrix[0] = -3;
		
	return matrix;
}

/* nice print our matrix. not much here, also one of the costly functions */
void print_matrix(const int *matrix,const int N)
{
	int i, j;
	for (i = 0; i < N*N; i += N)
	{
		for (j = 0; j < N; j++)
		{
			printf("%d\t", matrix[i + j]);

		}
		printf("\n");
	}
}