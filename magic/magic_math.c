#include "magic_math.h"

/*Compute and return sum of given row*/
/*get pointer to array and number of integers in it*/
int comp_row(const int *row, const int N)
{
	int sum = 0;
	int i;
	for (i = 0; i < N; i++)
		sum += (*(row + i));
	return sum;
}

/*compute and return sum of given column*/
/*get pointer to array and number of integers in it*/
int comp_col(const int *col, const int N)
{
	int sum = 0;
	int i;
	for (i = 0; i < N*N; i+=N)
		sum += (*(col + i));
	return sum;
}
/*compute and return sum of first diagonal from top left corner to bottomright corner*/
/*recieve pointer to array and number of integers in it*/
int comp_diag1(const int *diag, const int N)
{
	int sum = 0;
	int i;
	for (i = 0; i < N*N; i=i+N+1)
		sum += (*(diag + i));
	return sum;
}

/*compute and return sum of second diagonal from top right corner to bottom left corner*/
/*recieve pointer to array and number of integers in it*/
int comp_diag2(const int *diag, const int N)
{
	int sum = 0;
	int i;
	for (i = 0; i < N*N-N; i = i + N - 1)
		sum += (*(diag + i));
	return sum;
}

/*check if there are equal integers in the array. One of the most costly functions*/
/*recieve pointer to array and number of integers in it*/
int check_matrix_nums(const int *matrix, const int N)
{
	int powN = N*N;
	int i, j;
	
	for (i = 0; i < powN; i++)
		for (j = i + 1; j < powN; j++)
			if (matrix[i] == matrix[j])
				return -1;
	return 0;
}

/*check if given matrix is a magic square*/
/*recieve pointer to array and number of integers in it*/
/*using all of the above functions*/
int comp_magic(const int* matrix, const int N)
{
	/*get sum to check with, can be done within if check but for me it is more readable*/
	int sum = comp_row(matrix, N);
	int i = 0;
	int j = 0;
	/*check diagonals*/
	if (sum != comp_diag1(matrix, N) || sum != comp_diag2((int*)matrix + N - 1, N))
		return -1;
	/*check all the rows and columns*/
	for (i = 0; i < N; i++)
	{
		if (sum != comp_row((int*)matrix+j, N) || sum != comp_col((int*)matrix+i, N))
			return -1;
		j+=N; /*advance rows*/
	}
	return 0;
}